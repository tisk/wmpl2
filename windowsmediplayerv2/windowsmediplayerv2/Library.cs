﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
//using System.Collections.ObjectModel;
//using System.Windows.Forms.MessageBox;

namespace windowsmediplayerv2
{
    public class Library
    {
        public string libraryfile = "librarysave.xml";
        public string playlistfile = "playlistsave.xml";
        public string defaultplaylist = "default";

        [XmlElement("Video", Type = typeof(Video))]
        [XmlElement("Sound", Type = typeof(Audio))]
        [XmlElement("Picture", Type = typeof(Picture))]
        public ObservableCollection<AMedia> listMedia = new ObservableCollection<AMedia>();
        public ObservableCollection<Playlist> listPlaylist = new ObservableCollection<Playlist>();
        public ObservableCollection<AMedia> listMediaToDraw = new ObservableCollection<AMedia>();

        string currentPlayListName;

        ~Library()
        {
            SerializePlaylistToXML();
            SerializeToXML();
        }

        public bool isPlayListEmpty(/*int id*/)
        {
            return ((listPlaylist.Last().isEmpty()) ? true : false);
        }

        public void delCurrentPlayList(/*int id*/)
        {
            this.listPlaylist.RemoveAt(this.listPlaylist.Count - 1);
            if (this.listPlaylist.Count == 0)
                currentPlayListName = null;
        }

        public void Initialize()
        {
            Playlist pl = new Playlist();

            DeSerializeFromXml();
            currentPlayListName = defaultplaylist;
            if (getDefaultPlaylist() == null)
            {
                pl.setName(defaultplaylist);
                listPlaylist.Add(pl);
            }
        }

        public Playlist getDefaultPlaylist()
        {
            foreach (Playlist pl in listPlaylist)
            {
                if (pl.getName() == defaultplaylist)
                    return (pl);
            }
            return null;
        }

        public Playlist getCurrentPlayList()
        {
            foreach (Playlist pl in listPlaylist)
            {
                if (pl.getName() == currentPlayListName)
                    return (pl);
            }
            return null;
        }

        public Playlist getPlaylist(String plName)
        {
            foreach (Playlist pl in listPlaylist)
            {
                if (pl.name == plName)
                    return (pl);
            }
            return (null);
        }

        public string getPosList()
        {
            return this.currentPlayListName;
        }

        public void DeletePlaylist(int index)
        {
            if (index > 0 && index < this.listPlaylist.Count)
            {
                this.listPlaylist.RemoveAt(index);
            }
        }

        #region AddMediaToLib
        public int GetMediaType(string Filename)
        {
            if (Filename != "")
            {
                try
                {
                    TagLib.File tagFile = TagLib.File.Create(Filename);
                    if (tagFile.Properties.MediaTypes.HasFlag(TagLib.MediaTypes.Audio) && (tagFile.Properties.MediaTypes.HasFlag(TagLib.MediaTypes.Video)))
                        return (1);
                    if (tagFile.Properties.MediaTypes.HasFlag(TagLib.MediaTypes.Audio))
                        return (2);
                    if (tagFile.Properties.MediaTypes.HasFlag(TagLib.MediaTypes.Photo))
                        return (0);
                }
                catch (TagLib.UnsupportedFormatException)
                {
                    System.Windows.Forms.MessageBox.Show("Format incorrect", "Error message", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
            }
            return (-1);
        }

        public void GetMediaInfo(string Filename)
        {
            try
            {
                TagLib.File tagFile = TagLib.File.Create(Filename);
                this.listMedia.Last().setAlbum(tagFile.Tag.Album);
                if (GetMediaType(Filename) == 2)
                {
                    if (tagFile.Tag.AlbumArtists != null)
                    {
                        this.listMedia.Last().setAuthor(tagFile.Tag.AlbumArtists[0]);
                    }
                    if (tagFile.Tag.AlbumArtists != null)
                    {
                        this.listMedia.Last().setGenre(tagFile.Tag.Genres[0]);
                    }
                }
                this.listMedia.Last().setLenght(tagFile.Properties.Duration);
            }
            catch (TagLib.UnsupportedFormatException)
            {
                System.Windows.Forms.MessageBox.Show("Format incorrect", "Error message", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        public void addElemToMediaList(string Filename)
        {
            if (GetMediaType(Filename) != -1)
            {
                if (!ExistInLibrary(Filename))
                {
                    if (GetMediaType(Filename) == 0)
                        this.listMedia.Add(new Picture());
                    if (GetMediaType(Filename) == 1)
                        this.listMedia.Add(new Video());
                    if (GetMediaType(Filename) == 2)
                        this.listMedia.Add(new Audio());
                    this.listMedia.Last().setName(Filename.Split('\\').Last<String>().Split('.').First<String>());
                    this.listMedia.Last().setPath(Filename);
                    this.listMedia.Last().setType(Filename.Split('\\').Last<String>().Split('.').Last<String>());
                    GetMediaInfo(Filename);
                    SerializeToXML();
                }
                //ici on check la non existence d'une current playlist
                if (currentPlayListName == null || isListPlayListEmpty() || getCurrentPlayList() == null)
                {
                    string playlistName = "";

                    playlistName = Microsoft.VisualBasic.Interaction.InputBox("Enter playlist name : ", "IntputBox", "", -1, -1);
                    if (!AddPlaylistToLib(playlistName))
                    {
                        return;
                    }
                    //SerializePlaylistToXML();
                   // this.listPlaylist.Add(new Playlist());
                }
                for (int i = 0; i < listMedia.Count; i++)
                {
                    if (listMedia[i].path == Filename)
                    {
                        this.getCurrentPlayList().addElemToPlayList(listMedia[i]);
                    }
                }
            }
        }

        public string getPath(string name)
        {
            for (int i = 0; i < listMedia.Count; i++)
            {
                if (listMedia[i].name == name)
                    return listMedia[i].path;
            }
            return null;
        }

        public bool isListPlayListEmpty()
        {
            if (this.listPlaylist.Count <= 0)
                return true;
            return false;
        }

        public void SelectMediaType(int typeA, int typeB, int typeC)
        {
            this.listMediaToDraw.Clear();
            foreach (AMedia media in listMedia)           
              if (media.getElemType() == typeA || media.getElemType() == typeB || media.getElemType() == typeC)
                 this.listMediaToDraw.Add(media);           
        }

        public bool ExistInLibrary(string Filename)
        {
            foreach (AMedia media in listMedia)
            {
                if (Filename.Split('\\').Last<String>().Split('.').First<String>().CompareTo(media.getName()) == 0)
                    return true;
            }
            return false;
        }
        #endregion AddMediaToLib

        #region Playlist Method
        public bool AddPlaylistToLib(string playlistName)
        {            
            Playlist toAdd = new Playlist();

            if (playlistName == "")
                return (false);
            foreach (Playlist playlist in listPlaylist)
            {
                if (playlist.getName() == playlistName)
                    return (false);
            }
            if (listPlaylist.Count == 0)
            {
                listPlaylist.Add(new Playlist());
                listPlaylist.Last().setName(playlistName);
                currentPlayListName = playlistName;
            }
            else
            {
                if (getCurrentPlayList().ListMedia.Count == 0)
                    return (false);
                foreach (AMedia m in getCurrentPlayList().ListMedia)
                {
                    toAdd.ListMedia.Add(m);
                }
                toAdd.setName(playlistName);
                listPlaylist.Add(toAdd);
                toAdd.delEventToMedia();
            }
            return (true);
        }
        #endregion AddPlaylistToLib

        #region Serialization
        public void SerializeToXML()
        {
            // Serialization de la librairie
            XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<AMedia>));
            TextWriter textWriter = new StreamWriter(libraryfile);
            serializer.Serialize(textWriter, listMedia);
            textWriter.Close();
        }

        public void SerializePlaylistToXML()
        {
            // Serialization des playliste   
            XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<Playlist>));
            TextWriter textWriter = new StreamWriter(playlistfile);
            serializer.Serialize(textWriter, listPlaylist);
            textWriter.Close();
        }

        public void DeSerializeFromXml()
        {
            if (File.Exists(libraryfile))
            {
                // Deserialization de la librairie
                XmlSerializer deserializer = new XmlSerializer(typeof(ObservableCollection<AMedia>));
                TextReader textReader = new StreamReader(libraryfile);
                listMedia = (ObservableCollection<AMedia>)deserializer.Deserialize(textReader);
                textReader.Close();
            }
            if (File.Exists(playlistfile))
            {
                // Deserialization des playlist
                XmlSerializer deserializer = new XmlSerializer(typeof(ObservableCollection<Playlist>));
                TextReader textReader = new StreamReader(playlistfile);
                listPlaylist = (ObservableCollection<Playlist>)deserializer.Deserialize(textReader);
                textReader.Close();
            }
        }
        #endregion
    }
}
