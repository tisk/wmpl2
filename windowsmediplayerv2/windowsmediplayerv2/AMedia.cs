﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace windowsmediplayerv2
{
    [XmlInclude(typeof(Video))]
    [XmlInclude(typeof(Audio))]
    [XmlInclude(typeof(Picture))]
    public abstract class AMedia
    {
        /* 
         * Pour l'instant les attributs sont en public
         * */
         
        [XmlElement("Name")]
        public string name { get; set; }
        public void setName(string name) { this.name = name; }
        public string getName() { return this.name; }

        [XmlElement("Path")]
        public string path { get; set; }
        public void setPath(string path) { this.path = path; }
        public string getPath() { return this.path; }

        [XmlElement("Type")]
        public string mediaType { get; set; }
        public void setType(string type) { this.mediaType = type; }
        public string getType() { return this.mediaType; }

        public int Id { get; set; }
        public void setId(int id) { this.Id = id; }
        public int getId() { return this.Id; }

        private int type { get; set; }
        private bool loaded { get; set; }

        public void init() { loaded = false; type = 0; name = ""; }
        public virtual int getElemType() { return -1; }
        public virtual void setNewActionControl(EventHandler<actionArg> e) { }

        [XmlElement("Lenght")]
        public TimeSpan lenght { get; set; }
        public void setLenght(TimeSpan len) { lenght = len; }
        public TimeSpan getLenght() { return lenght; }

        [XmlElement("Author")]
        public string author { get; set; }
        public void setAuthor(string au) { author = au; }
        public string getAuthor() { return author; }

        [XmlElement("Album")]
        public string album { get; set; }
        public void setAlbum(string al) { album = al; }
        public string getAlbum() { return album; }

        [XmlElement("Genre")]
        public string genre { get; set; }
        public void setGenre(string ge) { genre = ge; }
        public string getGenre() { return genre; }

        public virtual void playPause()
        {

        }
        public virtual void stop()
        {

        }
        public virtual void load()
        {

        }
        public virtual void forwad()
        {

        }
        public virtual void backward()
        {

        }
        public virtual void changeVolume()
        {

        }
        public virtual void resetActionControl()
        {

        }
    }
}
