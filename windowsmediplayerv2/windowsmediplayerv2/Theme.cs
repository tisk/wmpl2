﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace windowsmediplayerv2
{
    [Serializable]
    public class Theme
    {
        private SolidColorBrush mySolidColorBrush = new SolidColorBrush();

        public byte[] _MainWindow = new byte[3];
        public byte[] _CurrentTab = new byte[3];
        public byte[] _Tabs = new byte[3];
        public byte[] _PlaylistPanel = new byte[3];
        public byte[] _Font = new byte[3];

        public  Theme()
        {

        }

        private void setRGBtoList(ref byte[] mylist, byte R, byte G, byte B)
        {
            mylist[0] = R;
            mylist[1] = G;
            mylist[2] = B;
        }

        public void  applyMainWindow(byte R, byte G, byte B)
        {
            setRGBtoList(ref _MainWindow, R, G, B);
        }

        public void applyCurrentTab(byte R, byte G, byte B)
        {
            setRGBtoList(ref _CurrentTab, R, G, B);
        }

        public void applyTabs(byte R, byte G, byte B)
        {
            setRGBtoList(ref _Tabs, R, G, B);
        }

        public void applyPanel(byte R, byte G, byte B)
        {
            setRGBtoList(ref _PlaylistPanel, R, G, B);
        }

        public void applyFont(byte R, byte G, byte B)
        {
            setRGBtoList(ref _Font, R, G, B);
        }

        public void applyAll(byte R, byte G, byte B)
        {
            applyCurrentTab(R, G, B);
            applyFont(R, G, B);
            applyMainWindow(R, G, B);
            applyPanel(R, G, B);
            applyTabs(R, G, B);
        }

        public void applyPredefine(byte R, byte G, byte B, byte R2, byte G2, byte B2)
        {
            applyCurrentTab(R2, G2, B2);
            applyFont(0, 0, 0);
            applyMainWindow(R, G, B);
            applyPanel(R2, G2, B2);
            applyTabs(R2, G2, B2);
        }

        #region Serialization

        public void SerializeToXML(string xmlfile)
        {
            if (xmlfile != "")
            {
                TextWriter textWriter = new StreamWriter(xmlfile);                
                try
                {
                    // Make sure even the construsctor runs inside a
                    // try-catch block
                    XmlSerializer serializer = new XmlSerializer(typeof(Theme));
                    serializer.Serialize(textWriter, this);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                textWriter.Close();
            }
        }
        #endregion
    }
}
