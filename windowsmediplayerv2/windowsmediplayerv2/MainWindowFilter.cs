﻿using System;
using windowsmediplayerv2;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
/*using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
*/
namespace windowsmediplayerv2
{
    public partial class MainWindow : Window
    {
        private String selection = "";
        private Expression expr = new Expression();

        private void SimpleMode(object sender, RoutedEventArgs e)
        {
            Mode.IsChecked = true;
            ModeChanged();
        }

        private void AdvancedMode(object sender, RoutedEventArgs e)
        {
            Mode.IsChecked = false;
            ModeChanged();
        }

        private void ModeChanged()
        {
            bool value = (bool)!Mode.IsChecked;

            Type.IsEnabled = value;
            NotCondition.IsEnabled = value;
            Condition.IsEnabled = value;
            NoLogic.IsEnabled = value;
            And.IsEnabled = value;
            Or.IsEnabled = value;
            None.IsEnabled = value;
            OpenPar.IsEnabled = value;
            ClosePar.IsEnabled = value;
            Del.IsEnabled = value;
            Add.IsEnabled = value;
            Clear.IsEnabled = value;
        }

        private int indexToInsert()
        {
            return Command.Items.Count;
        }

        private void addAndOr(System.Windows.Controls.ListViewItem item, String sub)
        {
            String whenS = windowsmediplayerv2.Filter.pkeyword("when") + " ";
            String whenP = whenS + windowsmediplayerv2.Filter.pkeyword("(");
            int i;

            if (NoLogic.IsChecked == false)
            {
                item.Content = (And.IsChecked == true ? ":and " : ":or ").Replace(":", "");
                Command.Items.Insert(indexToInsert(), item);
            }
            if (sub.IndexOf(whenP) != -1)
            {
                item = new System.Windows.Controls.ListViewItem();
                item.Content = windowsmediplayerv2.Filter.keyword("(");
                Command.Items.Insert(indexToInsert(), item);
            }
            item = new System.Windows.Controls.ListViewItem();
            if (OpenPar.IsChecked == true)
                item.Content = sub.Substring(sub.IndexOf(whenP) + whenP.Length).Replace(":", "");
            else if (ClosePar.IsChecked == true)
            {
                i = sub.IndexOf(whenS) + whenS.Length;
                item.Content = sub.Substring(i, sub.IndexOf(" " + windowsmediplayerv2.Filter.pkeyword(")")) - i).Replace(windowsmediplayerv2.Filter.pchar(), "");
                Command.Items.Insert(indexToInsert(), item);
                item = new System.Windows.Controls.ListViewItem();
                item.Content = windowsmediplayerv2.Filter.keyword(")");
            }
            else
                item.Content = sub.Substring(sub.IndexOf(whenS) + whenS.Length).Replace(windowsmediplayerv2.Filter.pchar(), "");
            Command.Items.Insert(indexToInsert(), item);
        }

        private void addWhen(System.Windows.Controls.ListViewItem item)
        {
            String when = windowsmediplayerv2.Filter.pkeyword("when") + " " + windowsmediplayerv2.Filter.pkeyword("(");

            item.Content = windowsmediplayerv2.Filter.keyword("when");
            Command.Items.Insert(indexToInsert(), item);
            if (selection.IndexOf(when) != -1)
            {
                item = new System.Windows.Controls.ListViewItem();
                item.Content = windowsmediplayerv2.Filter.keyword("(");
                Command.Items.Insert(indexToInsert(), item);
                item = new System.Windows.Controls.ListViewItem();
                item.Content = selection.Substring(selection.IndexOf(when) + when.Length).Replace(windowsmediplayerv2.Filter.pchar(), "");
            }
            else
            {
                item = new System.Windows.Controls.ListViewItem();
                item.Content = selection.Substring(selection.IndexOf(windowsmediplayerv2.Filter.pkeyword("when")) +
                                                    windowsmediplayerv2.Filter.pkeyword("when").Length).Replace(windowsmediplayerv2.Filter.pchar(), "");
            }
            Command.Items.Insert(indexToInsert(), item);
        }

        private void insertSelection()
        {
            System.Windows.Controls.ListViewItem item = new System.Windows.Controls.ListViewItem();
            int i = indexToInsert();
            int cut = -1;
            String sub = selection.Substring((windowsmediplayerv2.Filter.pkeyword("select") + " ").Length);

            if (!((System.Windows.Controls.ListViewItem)Command.Items[i - 1]).Content.Equals(windowsmediplayerv2.Filter.keyword("select")))
                cut = sub.IndexOf(windowsmediplayerv2.Filter.pkeyword("when"));
            if (cut != -1)
            {
                addAndOr(item, sub);
                return;
            }
            else
            {
                if (selection.IndexOf(windowsmediplayerv2.Filter.pkeyword("when") + " "+ windowsmediplayerv2.Filter.pkeyword("(")) != -1 ||
                    selection.IndexOf(windowsmediplayerv2.Filter.pkeyword("when")) != -1)
                {
                    addWhen(item);
                    return;
                }
            }
            item.Content = sub.Replace(windowsmediplayerv2.Filter.pchar(), "");
            Command.Items.Insert(indexToInsert(), item);
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            selection = windowsmediplayerv2.Filter.pkeyword("select") + " ";
            if ((String)Condition.SelectionBoxItem != "")
            {
                selection += windowsmediplayerv2.Filter.pkeyword("when") + " " +
                    (OpenPar.IsChecked == true ? windowsmediplayerv2.Filter.pkeyword("(") + " " + windowsmediplayerv2.Filter.pchar() : "") +
                    Type.SelectionBoxItem + " " + windowsmediplayerv2.Filter.pchar() +
                    (NotCondition.IsChecked == true ? "not" : "") +
                    Condition.SelectionBoxItem + " \"" +
                    Comparison.Text + "\" " +
                    windowsmediplayerv2.Filter.pkeyword(CaseSensitiveBox.IsChecked == false ? "NotCaseSensitive" : "CaseSensitive") + " " +
                    (ClosePar.IsChecked == true ? windowsmediplayerv2.Filter.pkeyword(")") : "");
            }
            if ((String)Condition.SelectionBoxItem == "")
            {
                if (OpenPar.IsChecked == true)
                {
                    System.Windows.Controls.ListViewItem item = new System.Windows.Controls.ListViewItem();
                    if (And.IsChecked == true)
                    {
                        item.Content = windowsmediplayerv2.Filter.keyword("and");
                        Command.Items.Insert(indexToInsert(), item);
                        item = new System.Windows.Controls.ListViewItem();
                    }
                    if (Or.IsChecked == true)
                    {
                        item.Content = windowsmediplayerv2.Filter.keyword("or");
                        Command.Items.Insert(indexToInsert(), item);
                        item = new System.Windows.Controls.ListViewItem();
                    }
                    item.Content = windowsmediplayerv2.Filter.keyword("(");
                    Command.Items.Insert(indexToInsert(), item);
                }
                else if (ClosePar.IsChecked == true)
                {
                    System.Windows.Controls.ListViewItem item = new System.Windows.Controls.ListViewItem();
                    item.Content = windowsmediplayerv2.Filter.keyword(")");
                    Command.Items.Insert(indexToInsert(), item);
                }
                return;
            }
            insertSelection();
        }

        private void Del_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.ListViewItem item;
            while (0 < Command.SelectedItems.Count)
            {
                item = (System.Windows.Controls.ListViewItem)Command.SelectedItems[0];
                Command.Items.Remove(item);
                Command.SelectedItems.Remove(item);
            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            while (1 < Command.Items.Count)
                Command.Items.RemoveAt(1);
            //Found.Items.Clear();
            filter.mediaToDraw.Clear();
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            //Found.Items.Clear();
            filter.mediaToDraw.Clear();
            if (Mode.IsChecked == true)
                Ok_Simple();
            else
                Ok_Advanced();
        }

        private bool conditionSimple(AMedia media, List<String> tokens)
        {
            String name = media.name;
            String token;
            if (CaseSensitiveBox.IsChecked == false)
                name = name.ToLower();
            foreach (String token_it in tokens)
            {
                token = token_it;
                if (CaseSensitiveBox.IsChecked == false)
                    token = token.ToLower();
                if (name.Contains(token))
                    return true;
            }
            return false;
        }

        private void Ok_Simple()
        {
            List<String> tmp = new List<String>();
            IEnumerable<AMedia> res = null;
            Playlist playlist;

            foreach (String token in Comparison.Text.Split(' '))
                if (token != "")
                    tmp.Add(token);
            res = from media in library.listMedia where conditionSimple(media, tmp) == true select media;
            if ((playlist = library.getPlaylist("Selected")) == null)
            {
                playlist = new Playlist("Selected");
                library.listPlaylist.Add(playlist);
            }
            else
                playlist.ListMedia.Clear();
            foreach (AMedia media in res)
            {
                System.Windows.Controls.ListViewItem item = new System.Windows.Controls.ListViewItem();
                item.Content = media.name;
                playlist.addElemToPlayList(media);
                filter.mediaToDraw.Add(media);
               // Found.Items.Add(item);
            }
            //filter.Selected
        }

        

        private void Ok_Advanced()
        {
//            windowsmediplayerv2.Filter filter = new windowsmediplayerv2.Filter(ref library, Found);
            filter.caseSensitive = (bool)CaseSensitiveBox.IsChecked;
            int i = 0;

            selection = "";
            while (i < Command.Items.Count)
            {
                selection += ((System.Windows.Controls.ListViewItem)Command.Items[i]).Content;
                filter.addInTree((String)((System.Windows.Controls.ListViewItem)Command.Items[i]).Content);
                i++;
            }
            filter.addInTree(";");
            selection += ";";
            filter.execute(selection);
        }
    }
}
