﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Xml.Serialization;

namespace windowsmediplayerv2
{
    public partial class MainWindow : Window
    {
        #region declarations
        private bool isFullScreen = false;
        private bool isMute = false;
        private bool isPlaylist = false;
        private bool isControl = false;
        private double oldVolume;
        private Timer timer;
        private bool justMod;
        private bool first;
        private MediaElementControl mediaControl;
        private Library library = new Library();
        private Theme theme;
        private string defaultThemeXML = "themes\\defaultTheme.xml";
        public Filter filter;
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            this.timer = new Timer();
            this.timer.Interval = (int)TimeSpan.FromMilliseconds(200).TotalMilliseconds;
            this.timer.Tick += new EventHandler(MediaTimeChanged);
            this.timer.Start();
            this.theme = new Theme();

            if (File.Exists(defaultThemeXML))
            {
                defaultTheme();
            }
            else
            {
                GreyTheme(null, null);
            }
            mediaControl = new MediaElementControl(ref this.mediaElement1, ref this.listViewLateral, ref this.library);
            first = true;
            justMod = true;

            this.library.Initialize();
            playlistContent.ItemsSource = this.library.listPlaylist;
            listView2.ItemsSource = this.library.listMediaToDraw;
            this.listViewLateral.ItemsSource = this.library.getCurrentPlayList().ListMedia;

            listView2.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.handle_listview2_dbleClick);
            listViewLateral.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.handle_listviewlateral_dbleClick);
            playlistContent.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.handle_playlistContent_dbleClick);
            Found.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.handle_found_dbleClick);
            repeat.Click += new System.Windows.RoutedEventHandler(this.handle_repeat_click);
            this.library.SelectMediaType(
               (!isImageRect) ? 0 : -1,
               (!isVideoRect) ? 1 : -1,
               (!isMusicRect) ? 2 : -1);
            AdvancedMode(null, null);
            SimpleMode(null, null);
            filter = new Filter(ref library, Found);
            Found.ItemsSource = filter.mediaToDraw;
        }

        public void playStreaming()
        {
            this.mediaControl.loadNewMedia("");
            video_position.Value = 0;
            mediaElement1.LoadedBehavior = MediaState.Play;
            this.library.getCurrentPlayList().HasChanged = false;
        }

        #region buttons

        private void inputBoxStream(object sender, RoutedEventArgs e)
        {
            string msg = Microsoft.VisualBasic.Interaction.InputBox("Enter stream url: ", "IntputBox", "", -1, -1);
        }

        private void pause_video(object sender, RoutedEventArgs e)
        {
            this.mediaControl.hpause();
        }

        private void stop_video(object sender, RoutedEventArgs e)
        {
            if (this.library.isListPlayListEmpty() || library.getCurrentPlayList() == null)
                return;
            if (this.library.getCurrentPlayList().getCurrentMedia() != null)
            {
                this.library.getCurrentPlayList().getCurrentMedia().stop();
                this.mediaControl.stopPlay();
            }
        }

        private static List<String> FindAllFiles(string path)
        {
            List<String> files = new List<string>();

            foreach (String filepath in Directory.GetDirectories(path))
            {
                try
                {
                    foreach (String file in Directory.GetFiles(filepath))
                        files.Add(file);
                }
                catch (Exception err)
                {
                    Console.WriteLine(err.Message);
                }
            }
            foreach (String dir in Directory.GetDirectories(path))
                try
                {
                    files.AddRange(FindAllFiles(dir));
                }
                catch (Exception err)
                {
                    Console.WriteLine(err.Message);
                }
            return files;
        }

        private void load_file(String filename)
        {
            this.library.addElemToMediaList(filename);

            if (first)
            {
                first = false;
                this.library.getCurrentPlayList().addEventToMedia(new EventHandler<actionArg>(this.actionControl_control));
                this.listViewLateral.ItemsSource = this.library.getCurrentPlayList().ListMedia;
                if (this.library.getCurrentPlayList().getCurrentMedia() != null)
                    this.library.getCurrentPlayList().getCurrentMedia().playPause();
            }
            this.library.SelectMediaType(
                           (!isImageRect) ? 0 : -1,
                           (!isVideoRect) ? 1 : -1,
                           (!isMusicRect) ? 2 : -1);
        }

        private void load_folder(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog ofd;
            ofd = new FolderBrowserDialog();
            ofd.ShowDialog();
            Console.WriteLine(ofd.SelectedPath);
            if (ofd.SelectedPath != "")
            {
                foreach (String file in FindAllFiles(ofd.SelectedPath))
                    load_file(file);
            }
        }

        private void load_video(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd;
            ofd = new OpenFileDialog();
            ofd.AddExtension = true;
            ofd.DefaultExt = "*.*";
            ofd.Filter = "Media (*.*)|*.*";
            ofd.ShowDialog();
            if (ofd.FileName != "")
                load_file(ofd.FileName);
        }

        private void load_video_clipboard(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd;
            ofd = new OpenFileDialog();
            ofd.InitialDirectory = System.Windows.Clipboard.GetText(System.Windows.TextDataFormat.Text);
            ofd.AddExtension = true;
            ofd.DefaultExt = "*.*";
            ofd.Filter = "Media (*.*)|*.*";
            ofd.ShowDialog();

            if (ofd.FileName != "")
            {
                this.library.addElemToMediaList(ofd.FileName);
                if (first)
                {
                    first = false;
                    this.library.getCurrentPlayList().addEventToMedia(new EventHandler<actionArg>(this.actionControl_control));
                    this.listViewLateral.ItemsSource = this.library.getCurrentPlayList().ListMedia;
                }
            }
        }

        private void play_video(object sender, RoutedEventArgs e)
        {
            if (this.library.isListPlayListEmpty() || library.getCurrentPlayList() == null)
                return;
            if (this.library.getCurrentPlayList().getCurrentMedia() != null)
                this.library.getCurrentPlayList().getCurrentMedia().playPause();
        }

        private void listView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {            
        }

        private void volume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            mediaElement1.Volume = volume.Value;
        }

        private void avance_rapide(object sender, RoutedEventArgs e)
        {
            if (this.library.isListPlayListEmpty() || library.getCurrentPlayList() == null)
                 return;
            if (library.getCurrentPlayList().getCurrentMedia() != null)
                library.getCurrentPlayList().getCurrentMedia().forwad();
        }

        private void retour_rapide(object sender, RoutedEventArgs e)
        {
            if (this.library.isListPlayListEmpty() || library.getCurrentPlayList() == null)
                return;
            if (library.getCurrentPlayList().getCurrentMedia() != null)
                library.getCurrentPlayList().getCurrentMedia().backward();
        }

        private void fullScreen(object sender, RoutedEventArgs e)
        {
            if (isFullScreen == false)
            {
                this.WindowStyle = WindowStyle.None;
                this.WindowState = WindowState.Maximized;
                this.gridControl.Visibility = Visibility.Collapsed;
                controlDisplayRect.Visibility = Visibility.Visible;
            }
            else
            {
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                this.WindowState = WindowState.Normal;
                this.gridControl.Visibility = Visibility.Visible;
                controlDisplayRect.Visibility = Visibility.Collapsed;
            }
            isFullScreen = !isFullScreen;
        }

        private void myMuteSound(object sender, EventArgs e)
        {
            BitmapImage logo = new BitmapImage();

            logo.BeginInit();
            if (!isMute)
            {
                logo.UriSource = new Uri(@"assets/mute.png", UriKind.Relative);
                logo.EndInit();
                mute.Source = logo;
                oldVolume = mediaElement1.Volume;
                mediaElement1.Volume = 0;
            }
            else
            {
                logo.UriSource = new Uri(@"assets/volume.png", UriKind.Relative);
                logo.EndInit();
                mute.Source = logo;
                mediaElement1.Volume = oldVolume;
            }
            isMute = !isMute;
        }

        private void playPause(object sender, EventArgs e)
        {
             if (this.library.isListPlayListEmpty() || this.library.getCurrentPlayList() == null)
                return;
            if (this.library.getCurrentPlayList().getCurrentMedia() != null)
                this.library.getCurrentPlayList().getCurrentMedia().playPause();
        }

        private void displayPlaylist(object sender, EventArgs e)
        {
            if (isPlaylist)
                gridLateral.Visibility = Visibility.Collapsed;
            else
                gridLateral.Visibility = Visibility.Visible;
            isPlaylist = !isPlaylist;
        }

        private void displayControl(object sender, EventArgs e)
        {
            if (isControl)
                gridControl.Visibility = Visibility.Collapsed;
            else
                gridControl.Visibility = Visibility.Visible;
            isControl = !isControl;
        }

        private void displayControlEnter(object sender, EventArgs e)
        {
            if (isFullScreen)
            {
                gridControl.Visibility = Visibility.Visible;
                controlDisplayRect.Visibility = Visibility.Collapsed;
                isControl = !isControl;
            }
        }

        private void displayControlLeave(object sender, EventArgs e)
        {
            if (isFullScreen)
            {
                gridControl.Visibility = Visibility.Collapsed;
                controlDisplayRect.Visibility = Visibility.Visible;
                isControl = !isControl;
            }
        }

        private void changePlayPauseImage()
        {
            BitmapImage logo = new BitmapImage();

            logo.BeginInit();
            if (!this.mediaControl.isInPlay())
            {
                logo.UriSource = new Uri(@"assets/play.png", UriKind.Relative);
                logo.EndInit();
                playPauseImg.Source = logo;
            }
            else
            {
                logo.UriSource = new Uri(@"assets/pause.png", UriKind.Relative);
                logo.EndInit();
                playPauseImg.Source = logo;
            }
        }

        private void exitProgram(object sender, EventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        public void handle_repeat_click(object sender, System.EventArgs e)
        {
            if (!this.library.isListPlayListEmpty())
                if (this.library.getCurrentPlayList() != null)
                {
                    if (repeat.IsChecked == true)
                        this.library.getCurrentPlayList().setRepeat(true);
                    else
                        this.library.getCurrentPlayList().setRepeat(false);
                }
        }

        public void handle_found_dbleClick(object sender, System.EventArgs e)
        {
            if (Found.SelectedItem == null)
                return;
            AMedia test = (AMedia)Found.SelectedItem;
            this.library.addElemToMediaList(this.library.getPath(test.name));
            if (first)
            {
                first = false;
                this.library.getCurrentPlayList().addEventToMedia(new EventHandler<actionArg>(this.actionControl_control));
                this.listViewLateral.ItemsSource = this.library.getCurrentPlayList().ListMedia;
            }
        }

        public void handle_listview1_dbleClick(object sender, System.EventArgs e)
        {
            if (listViewLateral.SelectedItem == null)
                return;
            AMedia test = (AMedia)listViewLateral.SelectedItem;
            this.library.getCurrentPlayList().setListPos(test.getId());
            this.library.getCurrentPlayList().addEventToMedia(new EventHandler<actionArg>(this.actionControl_control));
            this.mediaControl.loadNewMedia(this.library.getCurrentPlayList().getCurrentMedia().getPath());
            video_position.Value = 0;
            this.library.getCurrentPlayList().getCurrentMedia().playPause();
        }

        public void handle_listviewlateral_dbleClick(object sender, System.EventArgs e)
        {
            if (listViewLateral.SelectedItem == null)
                return;
            AMedia test = (AMedia)listViewLateral.SelectedItem;
            this.library.getCurrentPlayList().setListPos(test.getId());
            this.library.getCurrentPlayList().addEventToMedia(new EventHandler<actionArg>(this.actionControl_control));
            this.mediaControl.loadNewMedia(this.library.getCurrentPlayList().getCurrentMedia().getPath());
            video_position.Value = 0;
            this.library.getCurrentPlayList().getCurrentMedia().playPause();
        }

        public void handle_listview2_dbleClick(object sender, System.EventArgs e)
        {
            if (listView2.SelectedItem == null)
                return;
            AMedia test = (AMedia)listView2.SelectedItem;
            this.library.addElemToMediaList(this.library.getPath(test.name));
            if (first)
            {
                first = false;
                this.library.getCurrentPlayList().addEventToMedia(new EventHandler<actionArg>(this.actionControl_control));
                this.listViewLateral.ItemsSource = this.library.getCurrentPlayList().ListMedia;
            }

        }

        public void handle_playlistContent_dbleClick(object sender, System.EventArgs e)
        {
            if (playlistContent.SelectedItem == null)
                return;
            Playlist test = (Playlist) playlistContent.SelectedItem;
            this.listViewLateral.ItemsSource = test.ListMedia;
        }        


        void BackwardPlayList(object sender, EventArgs e)
        {
            if (this.library.isListPlayListEmpty() || this.library.getCurrentPlayList() == null)
                return;
            this.library.getCurrentPlayList().updatePlayListPos(-1);
            this.library.getCurrentPlayList().addEventToMedia(new EventHandler<actionArg>(this.actionControl_control));
            this.mediaControl.loadNewMedia(this.library.getCurrentPlayList().getCurrentMedia().getPath());
            video_position.Value = 0;
            mediaElement1.LoadedBehavior = MediaState.Pause;
            this.mediaControl.stopPlay();
            this.library.getCurrentPlayList().HasChanged = false;
            this.library.getCurrentPlayList().getCurrentMedia().playPause();
        }

        void ForewardPlayList(object sender, EventArgs e)
        {
            if (this.library.isListPlayListEmpty() || this.library.getCurrentPlayList() == null)
                return;
            this.library.getCurrentPlayList().updatePlayListPos(1);
            this.library.getCurrentPlayList().addEventToMedia(new EventHandler<actionArg>(this.actionControl_control));
            this.mediaControl.loadNewMedia(this.library.getCurrentPlayList().getCurrentMedia().getPath());
            video_position.Value = 0;
            mediaElement1.LoadedBehavior = MediaState.Pause;
            this.mediaControl.stopPlay();
            this.library.getCurrentPlayList().HasChanged = false;
            this.library.getCurrentPlayList().getCurrentMedia().playPause();
        }

        void DeletePlayList(object sender, EventArgs e)
        {
            if (this.library.isListPlayListEmpty(/*id de la current playlist*/) || this.library.isPlayListEmpty() ||
                    this.listViewLateral.SelectedItem == null)
                return;            
            if (this.library.getCurrentPlayList() != null && this.library.getCurrentPlayList().getCurrentMedia() != null)
                this.library.getCurrentPlayList().getCurrentMedia().stop();
            this.mediaControl.stopPlay();
            AMedia test = (AMedia)listViewLateral.SelectedItem;
            this.library.getCurrentPlayList().delItem(test.getId());
            if (this.library.isPlayListEmpty())
                this.library.delCurrentPlayList(/*id de la current playlist*/);
            if (this.library.isListPlayListEmpty(/*id de la current playlist*/))
                first = true;
        }

        void SavePlayList(object sender, EventArgs e)
        {
			string playlistName = "";

            playlistName = Microsoft.VisualBasic.Interaction.InputBox("Enter playlist name : ", "IntputBox", "", -1, -1);

            library.AddPlaylistToLib(playlistName);
            library.SerializePlaylistToXML();
        }

        void DeleteSelectedPlaylist_Click(object sender, EventArgs e)
        {
            int todel;

            Console.Write(">>>>>>>>>>>>>>>>>>>>>>>>> DELETE : ");
            todel = this.playlistContent.SelectedIndex;
            this.library.DeletePlaylist(todel);
        }

        void LoadPlayList(object sender, EventArgs e)
        {
        }

        #endregion
        #region event
        private void video_position_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
             if (this.library.isListPlayListEmpty() || library.getCurrentPlayList() == null || this.library.getCurrentPlayList().getCurrentMedia() == null)
                 return;
            if (this.library.getCurrentPlayList().getCurrentMedia().getElemType() == 0 || !mediaControl.isInPlay())
                return;
            if (justMod)
            {
                justMod = false;
                return;
            }
            mediaElement1.LoadedBehavior = MediaState.Pause;
            mediaElement1.Position = new TimeSpan(0, 0, 0, 0, (int)video_position.Value);
            mediaElement1.LoadedBehavior = MediaState.Play;
        }

        private void mediaElement1_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (!this.library.isListPlayListEmpty() && this.library.getCurrentPlayList() != null && this.library.getCurrentPlayList().getCurrentMedia() != null)
            {
                this.CurrentMediaName.Text = this.library.getCurrentPlayList().getCurrentMedia().getName();
                if (this.library.getCurrentPlayList().getCurrentMedia().getElemType() == 0)
                {
                    video_position.Maximum = 10000;
                    this.TotalTime.Text = TimeSpan.FromMilliseconds(video_position.Maximum).ToString();
                    return;
                }
            }
            else
                this.CurrentMediaName.Text = "streaming from : ";
            video_position.Maximum = mediaElement1.NaturalDuration.TimeSpan.TotalMilliseconds;
            this.TotalTime.Text = TimeSpan.FromMilliseconds(video_position.Maximum).ToString();
            mediaElement1.LoadedBehavior = MediaState.Pause;
            mediaElement1.Position = new TimeSpan(0, 0, 0, 0, (int)video_position.Value);
            mediaElement1.LoadedBehavior = MediaState.Play;
        }

        private void MediaTimeChanged(object sender, EventArgs e)
        {
            if (this.library.isListPlayListEmpty(/*id de la current playlist*/))
                return;
            if (this.library.getCurrentPlayList() == null || this.library.getCurrentPlayList().getCurrentMedia() == null)
                return;
            changePlayPauseImage();
            if (mediaElement1.LoadedBehavior == MediaState.Pause)
                return;
            if (!this.mediaControl.isLoaded)
                return;
            justMod = true;
            if (this.library.getCurrentPlayList().getCurrentMedia().getElemType() != 0)
                video_position.Value = mediaElement1.Position.TotalMilliseconds;
            else
                video_position.Value += 200;
            this.PassedTime.Text = TimeSpan.FromMilliseconds(video_position.Value).ToString();
            
            if (this.library.isListPlayListEmpty())
                return ;
            if (video_position.Value >= video_position.Maximum)
                this.library.getCurrentPlayList().updatePlayListPos();
            if (this.library.getCurrentPlayList().HasChanged)
            {
                this.mediaControl.loadNewMedia(this.library.getCurrentPlayList().getCurrentMedia().getPath());
                this.library.getCurrentPlayList().HasChanged = false;
                this.library.getCurrentPlayList().addEventToMedia(new EventHandler<actionArg>(this.actionControl_control));
                video_position.Value = 0;
            }
        }
        #endregion

        private void KeyAction(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                this.fullScreen(sender, e);
            else if (e.Key == Key.Space)
                this.playPause(sender, e);
            else if (e.Key == Key.O && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                this.load_video(sender, e);
            else if (e.Key == Key.V && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                this.load_video_clipboard(sender, e);
            else if (e.Key == Key.Q && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                this.exitProgram(sender, e);
        }

        #region libraryUI
        public bool isImageRect = false;
        public bool isVideoRect = false;
        public bool isMusicRect = false;

        
        void imageRectChange(object sender, EventArgs e)
        {
            BitmapImage logo = new BitmapImage();

            logo.BeginInit();
            if (!isImageRect)
            {
                logo.UriSource = new Uri(@"assets/imageNB.png", UriKind.Relative);
                logo.EndInit();
                imageRect.Source = logo;
            }
            else
            {
                logo.UriSource = new Uri(@"assets/image.png", UriKind.Relative);
                logo.EndInit();
                imageRect.Source = logo;
            }
            isImageRect = !isImageRect;
            this.library.SelectMediaType(
                (!isImageRect) ? 0 : -1, 
                (!isVideoRect) ? 1 : -1,
                (!isMusicRect) ? 2 : -1);
        }

        void videoRectChange(object sender, EventArgs e)
        {
            BitmapImage logo = new BitmapImage();

            logo.BeginInit();
            if (!isVideoRect)
            {
                logo.UriSource = new Uri(@"assets/videoNB.png", UriKind.Relative);
                logo.EndInit();
                videoRect.Source = logo;
            }
            else
            {
                logo.UriSource = new Uri(@"assets/video.png", UriKind.Relative);
                logo.EndInit();
                videoRect.Source = logo;
            }
            isVideoRect = !isVideoRect;
            this.library.SelectMediaType(
                (!isImageRect) ? 0 : -1,
                (!isVideoRect) ? 1 : -1,
                (!isMusicRect) ? 2 : -1);
        }

        void musicRectChange(object sender, EventArgs e)
        {
            BitmapImage logo = new BitmapImage();

            logo.BeginInit();
            if (!isMusicRect)
            {
                logo.UriSource = new Uri(@"assets/musicNB.png", UriKind.Relative);
                logo.EndInit();
                musicRect.Source = logo;
            }
            else
            {
                logo.UriSource = new Uri(@"assets/music.png", UriKind.Relative);
                logo.EndInit();
                musicRect.Source = logo;
            }
            isMusicRect = !isMusicRect;
            this.library.SelectMediaType(
                (!isImageRect) ? 0 : -1,
                (!isVideoRect) ? 1 : -1,
                (!isMusicRect) ? 2 : -1);
        }

        #endregion

        #region Themes

        #region Predefine Themes
        void RedTheme(object sender, EventArgs e)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = System.Windows.Media.Color.FromRgb(255, 0, 0); ;
            MainGrid.Background = mySolidColorBrush;
            controlRect.Fill = mySolidColorBrush;
            LateralButtonPlaylist.Background = mySolidColorBrush;
            SolidColorBrush mySolidColorBrush2 = new SolidColorBrush();
            mySolidColorBrush2.Color = System.Windows.Media.Color.FromRgb(255, 150, 150);
            MainTab.Background = mySolidColorBrush2;
            Filter.Background = mySolidColorBrush2;
            libraryGrid.Background = mySolidColorBrush2;
            listView2.Background = mySolidColorBrush2;
            gridLateral.Background = mySolidColorBrush2;
            listViewLateral.Background = mySolidColorBrush2;
            screenTabHead.Background = mySolidColorBrush2;
            libraryTabHead.Background = mySolidColorBrush2;
            filterTabHead.Background = mySolidColorBrush2;
            themeTabHead.Background = mySolidColorBrush2;
            playlistTabHead.Background = mySolidColorBrush2;
            Found.Background = mySolidColorBrush2;
            playlistGrid.Background = mySolidColorBrush2;
            playlistContent.Background = mySolidColorBrush2;
            theme.applyPredefine(255, 0, 0, 255, 150, 150);
        }

        void YellowTheme(object sender, EventArgs e)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = System.Windows.Media.Color.FromRgb(255, 255, 0);
            MainGrid.Background = mySolidColorBrush;
            controlRect.Fill = mySolidColorBrush;
            LateralButtonPlaylist.Background = mySolidColorBrush;
            SolidColorBrush mySolidColorBrush2 = new SolidColorBrush();
            mySolidColorBrush2.Color = System.Windows.Media.Color.FromRgb(255, 255, 150);
            MainTab.Background = mySolidColorBrush2;
            Filter.Background = mySolidColorBrush2;
            libraryGrid.Background = mySolidColorBrush2;
            listView2.Background = mySolidColorBrush2;
            gridLateral.Background = mySolidColorBrush2;
            listViewLateral.Background = mySolidColorBrush2; 
            screenTabHead.Background = mySolidColorBrush2;
            libraryTabHead.Background = mySolidColorBrush2;
            filterTabHead.Background = mySolidColorBrush2;
            themeTabHead.Background = mySolidColorBrush2;
            playlistTabHead.Background = mySolidColorBrush2;
            Found.Background = mySolidColorBrush2;
            playlistGrid.Background = mySolidColorBrush2;
            playlistContent.Background = mySolidColorBrush2;
            theme.applyPredefine(255, 255, 0, 255, 255, 150);
        }

        void LimeTheme(object sender, EventArgs e)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = System.Windows.Media.Color.FromRgb(0, 255, 0);
            MainGrid.Background = mySolidColorBrush;
            controlRect.Fill = mySolidColorBrush;
            LateralButtonPlaylist.Background = mySolidColorBrush;
            SolidColorBrush mySolidColorBrush2 = new SolidColorBrush();
            mySolidColorBrush2.Color = System.Windows.Media.Color.FromRgb(150, 255, 150);
            MainTab.Background = mySolidColorBrush2;
            Filter.Background = mySolidColorBrush2;
            libraryGrid.Background = mySolidColorBrush2;
            listView2.Background = mySolidColorBrush2;
            gridLateral.Background = mySolidColorBrush2;
            listViewLateral.Background = mySolidColorBrush2;
            screenTabHead.Background = mySolidColorBrush2;
            libraryTabHead.Background = mySolidColorBrush2;
            filterTabHead.Background = mySolidColorBrush2;
            themeTabHead.Background = mySolidColorBrush2;
            playlistTabHead.Background = mySolidColorBrush2;
            Found.Background = mySolidColorBrush2;
            playlistGrid.Background = mySolidColorBrush2;
            playlistContent.Background = mySolidColorBrush2;
            theme.applyPredefine(0, 255, 0, 150, 255, 150);
        }

        void BlueTheme(object sender, EventArgs e)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = System.Windows.Media.Color.FromRgb(0, 0, 255);
            MainGrid.Background = mySolidColorBrush;
            controlRect.Fill = mySolidColorBrush;
            LateralButtonPlaylist.Background = mySolidColorBrush;
            SolidColorBrush mySolidColorBrush2 = new SolidColorBrush();
            mySolidColorBrush2.Color = System.Windows.Media.Color.FromRgb(150, 150, 255);
            MainTab.Background = mySolidColorBrush2;
            Filter.Background = mySolidColorBrush2;
            libraryGrid.Background = mySolidColorBrush2;
            listView2.Background = mySolidColorBrush2;
            gridLateral.Background = mySolidColorBrush2;
            listViewLateral.Background = mySolidColorBrush2;
            screenTabHead.Background = mySolidColorBrush2;
            libraryTabHead.Background = mySolidColorBrush2;
            filterTabHead.Background = mySolidColorBrush2;
            themeTabHead.Background = mySolidColorBrush2;
            playlistTabHead.Background = mySolidColorBrush2;
            Found.Background = mySolidColorBrush2;
            playlistGrid.Background = mySolidColorBrush2;
            playlistContent.Background = mySolidColorBrush2;
            theme.applyPredefine(0, 0, 255, 150, 150, 255);
        }

        void MagentaTheme(object sender, EventArgs e)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = System.Windows.Media.Color.FromRgb(255, 0, 255);
            MainGrid.Background = mySolidColorBrush;
            controlRect.Fill = mySolidColorBrush;
            LateralButtonPlaylist.Background = mySolidColorBrush;
            SolidColorBrush mySolidColorBrush2 = new SolidColorBrush();
            mySolidColorBrush2.Color = System.Windows.Media.Color.FromRgb(255, 150, 255);
            MainTab.Background = mySolidColorBrush2;
            Filter.Background = mySolidColorBrush2;
            libraryGrid.Background = mySolidColorBrush2;
            listView2.Background = mySolidColorBrush2;
            gridLateral.Background = mySolidColorBrush2;
            listViewLateral.Background = mySolidColorBrush2;
            screenTabHead.Background = mySolidColorBrush2;
            libraryTabHead.Background = mySolidColorBrush2;
            filterTabHead.Background = mySolidColorBrush2;
            themeTabHead.Background = mySolidColorBrush2;
            playlistTabHead.Background = mySolidColorBrush2;
            Found.Background = mySolidColorBrush2;
            playlistGrid.Background = mySolidColorBrush2;
            playlistContent.Background = mySolidColorBrush2;
            theme.applyPredefine(255, 0, 255, 255, 150, 255);
        }

        void GreyTheme(object sender, EventArgs e)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = System.Windows.Media.Color.FromRgb(214, 214, 214);
            MainGrid.Background = mySolidColorBrush;
            controlRect.Fill = mySolidColorBrush;
            LateralButtonPlaylist.Background = mySolidColorBrush;
            SolidColorBrush mySolidColorBrush2 = new SolidColorBrush();
            mySolidColorBrush2.Color = System.Windows.Media.Color.FromRgb(255, 255, 255); ;
            MainTab.Background = mySolidColorBrush2;
            Filter.Background = mySolidColorBrush2;
            libraryGrid.Background = mySolidColorBrush2;
            listView2.Background = mySolidColorBrush2;
            gridLateral.Background = mySolidColorBrush2;
            listViewLateral.Background = mySolidColorBrush2;
            screenTabHead.Background = mySolidColorBrush2;
            libraryTabHead.Background = mySolidColorBrush2;
            filterTabHead.Background = mySolidColorBrush2;
            themeTabHead.Background = mySolidColorBrush2;
            playlistTabHead.Background = mySolidColorBrush2;
            Found.Background = mySolidColorBrush2;
            playlistGrid.Background = mySolidColorBrush2;
            playlistContent.Background = mySolidColorBrush2;
            theme.applyPredefine(214, 214, 214, 255, 255, 255);
        }

        #endregion

        void saveTheme(object sender, EventArgs e)
        {
            SaveFileDialog ofd;
            ofd = new SaveFileDialog();
            ofd.AddExtension = true;
            ofd.DefaultExt = "*.*";
            ofd.Filter = "Media (*.*)|*.*";
            ofd.ShowDialog();
            if (ofd.FileName != "")
                theme.SerializeToXML(ofd.FileName);
        }

        void saveDefaultTheme(object sender, EventArgs e)
        {
            theme.SerializeToXML(defaultThemeXML);
        }

        void loadTheme(object sender, EventArgs e)
        {
            OpenFileDialog ofd;
            ofd = new OpenFileDialog();
            ofd.AddExtension = true;
            ofd.DefaultExt = "*.*";
            ofd.Filter = "Media (*.*)|*.*";
            ofd.ShowDialog();

            if (ofd.FileName != "")
            {
                if (new FileInfo(ofd.FileName).Length == 0)
                    return;
                XmlSerializer deserializer = new XmlSerializer(typeof(Theme));
                TextReader textReader = new StreamReader(ofd.FileName);
                theme = (Theme)deserializer.Deserialize(textReader);
                textReader.Close();
                applyLoadTheme();
            }
        }

        void defaultTheme()
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(Theme));
            TextReader textReader = new StreamReader(defaultThemeXML);
            theme = (Theme)deserializer.Deserialize(textReader);
            textReader.Close();
            applyLoadTheme();
        }

        void applyLoadTheme()
        {
            //Main window
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = System.Windows.Media.Color.FromRgb(theme._MainWindow[0], theme._MainWindow[1], theme._MainWindow[2]);
            MainGrid.Background = mySolidColorBrush;
            controlRect.Fill = mySolidColorBrush;
            LateralButtonPlaylist.Background = mySolidColorBrush;

            //Current tab
            SolidColorBrush mySolidColorBrush2 = new SolidColorBrush();
            mySolidColorBrush2.Color = System.Windows.Media.Color.FromRgb(theme._CurrentTab[0], theme._CurrentTab[1], theme._CurrentTab[2]);
            MainTab.Background = mySolidColorBrush2;
            Filter.Background = mySolidColorBrush2;
            libraryGrid.Background = mySolidColorBrush2;
            Found.Background = mySolidColorBrush2;
            listView2.Background = mySolidColorBrush2;
            playlistGrid.Background = mySolidColorBrush2;
            playlistContent.Background = mySolidColorBrush2;

            //Tabs
            SolidColorBrush mySolidColorBrush3 = new SolidColorBrush();
            mySolidColorBrush3.Color = System.Windows.Media.Color.FromRgb(theme._Tabs[0], theme._Tabs[1], theme._Tabs[2]);
            screenTabHead.Background = mySolidColorBrush3;
            libraryTabHead.Background = mySolidColorBrush3;
            filterTabHead.Background = mySolidColorBrush3;
            themeTabHead.Background = mySolidColorBrush3;
            playlistTabHead.Background = mySolidColorBrush3;

            //Playlist Panel
            SolidColorBrush mySolidColorBrush4 = new SolidColorBrush();
            mySolidColorBrush4.Color = System.Windows.Media.Color.FromRgb(theme._PlaylistPanel[0], theme._PlaylistPanel[1], theme._PlaylistPanel[2]);
            gridLateral.Background = mySolidColorBrush4;
            listViewLateral.Background = mySolidColorBrush4;

            //fonts
            SolidColorBrush mySolidColorBrush5 = new SolidColorBrush();
            mySolidColorBrush5.Color = System.Windows.Media.Color.FromRgb(theme._Font[0], theme._Font[1], theme._Font[2]);
            TopMenu.Foreground = mySolidColorBrush5;
            MainTab.Foreground = mySolidColorBrush5;
            PLTitle.Foreground = mySolidColorBrush5;
            #region fontColorFilter
            And.Foreground = mySolidColorBrush5;
            Or.Foreground = mySolidColorBrush5;
            None.Foreground = mySolidColorBrush5;
            OpenPar.Foreground = mySolidColorBrush5;
            ClosePar.Foreground = mySolidColorBrush5;
            #endregion
            #region Buttons
            Apply.Foreground = mySolidColorBrush5;
            LoadTheme.Foreground = mySolidColorBrush5;
            SaveTheme.Foreground = mySolidColorBrush5;
            Del.Foreground = mySolidColorBrush5;
            Add.Foreground = mySolidColorBrush5;
            Ok.Foreground = mySolidColorBrush5;
            load.Foreground = mySolidColorBrush5;
            PLBackward.Foreground = mySolidColorBrush5;
            PLForeward.Foreground = mySolidColorBrush5;
            PLDelete.Foreground = mySolidColorBrush5;
            PLSave.Foreground = mySolidColorBrush5;
            //PLLoad.Foreground = mySolidColorBrush5;
            MakeDefault.Foreground = mySolidColorBrush5;
            TargetApply.Foreground = mySolidColorBrush5;
            #endregion
            #region tabHead
            screenTabHead.Foreground = mySolidColorBrush5;
            libraryTabHead.Foreground = mySolidColorBrush5;
            filterTabHead.Foreground = mySolidColorBrush5;
            themeTabHead.Foreground = mySolidColorBrush5;
            #endregion
            #region NameTime
            CurrentMediaName.Foreground = mySolidColorBrush5;
            CurrentMediaNameTitle.Foreground = mySolidColorBrush5;
            TimeTitle.Foreground = mySolidColorBrush5;
            PassedTime.Foreground = mySolidColorBrush5;
            TotalTime.Foreground = mySolidColorBrush5;
            #endregion
            #region lists
            listView2.Foreground = mySolidColorBrush5;
            listViewLateral.Foreground = mySolidColorBrush5;
            #endregion

        }

        void ThemePersoValueChanged(object sender, EventArgs e)
        {
            SolidColorBrush myBrush = new SolidColorBrush(System.Windows.Media.Color.FromRgb((byte)slideRed.Value, (byte)slideGreen.Value, (byte)slideBlue.Value));
            OverviewRect.Fill = myBrush;
        }

        void applyTheme(object sender, EventArgs e)
        {
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = System.Windows.Media.Color.FromRgb((byte)slideRed.Value, (byte)slideGreen.Value, (byte)slideBlue.Value);
            if (TargetApply.Text == "Main window")
            {
                theme.applyMainWindow((byte)slideRed.Value, (byte)slideGreen.Value, (byte)slideBlue.Value);
                MainGrid.Background = mySolidColorBrush;
                controlRect.Fill = mySolidColorBrush;
                LateralButtonPlaylist.Background = mySolidColorBrush;
            }
            else if (TargetApply.Text == "Current tab")
            {
                theme.applyCurrentTab((byte)slideRed.Value, (byte)slideGreen.Value, (byte)slideBlue.Value);
                MainTab.Background = mySolidColorBrush;
                Filter.Background = mySolidColorBrush;
                libraryGrid.Background = mySolidColorBrush;
                listView2.Background = mySolidColorBrush;
                playlistGrid.Background = mySolidColorBrush;
                playlistContent.Background = mySolidColorBrush;
                Found.Background = mySolidColorBrush;
            }
            else if (TargetApply.Text == "Tabs")
            {
                theme.applyTabs((byte)slideRed.Value, (byte)slideGreen.Value, (byte)slideBlue.Value);
                screenTabHead.Background = mySolidColorBrush;
                libraryTabHead.Background = mySolidColorBrush;
                filterTabHead.Background = mySolidColorBrush;
                themeTabHead.Background = mySolidColorBrush;
                playlistTabHead.Background = mySolidColorBrush;
            }
            else if (TargetApply.Text == "Playlist panel")
            {
                theme.applyPanel((byte)slideRed.Value, (byte)slideGreen.Value, (byte)slideBlue.Value);
                gridLateral.Background = mySolidColorBrush;
                listViewLateral.Background = mySolidColorBrush;
            }
            else if (TargetApply.Text == "Font color")
            {
                theme.applyFont((byte)slideRed.Value, (byte)slideGreen.Value, (byte)slideBlue.Value);
                TopMenu.Foreground = mySolidColorBrush;
                MainTab.Foreground = mySolidColorBrush;
                PLTitle.Foreground = mySolidColorBrush;
                #region fontColorFilter
                And.Foreground = mySolidColorBrush;
                Or.Foreground = mySolidColorBrush;
                None.Foreground = mySolidColorBrush;
                OpenPar.Foreground = mySolidColorBrush;
                ClosePar.Foreground = mySolidColorBrush;
                #endregion
                #region Buttons
                Apply.Foreground = mySolidColorBrush;
                LoadTheme.Foreground = mySolidColorBrush;
                SaveTheme.Foreground = mySolidColorBrush;
                Del.Foreground = mySolidColorBrush;
                Add.Foreground = mySolidColorBrush;
                Ok.Foreground = mySolidColorBrush;
                load.Foreground = mySolidColorBrush;
                PLBackward.Foreground = mySolidColorBrush;
                PLForeward.Foreground = mySolidColorBrush;
                PLDelete.Foreground = mySolidColorBrush;
                PLSave.Foreground = mySolidColorBrush;
                //PLLoad.Foreground = mySolidColorBrush;
                MakeDefault.Foreground = mySolidColorBrush;
                TargetApply.Foreground = mySolidColorBrush;
                #endregion
                #region tabHead
                screenTabHead.Foreground = mySolidColorBrush;
                libraryTabHead.Foreground = mySolidColorBrush;
                filterTabHead.Foreground = mySolidColorBrush;
                themeTabHead.Foreground = mySolidColorBrush;
                #endregion
                #region NameTime
                CurrentMediaName.Foreground = mySolidColorBrush;
                CurrentMediaNameTitle.Foreground = mySolidColorBrush;
                TimeTitle.Foreground = mySolidColorBrush;
                PassedTime.Foreground = mySolidColorBrush;
                TotalTime.Foreground = mySolidColorBrush;
                #endregion
                #region lists
                listView2.Foreground = mySolidColorBrush;
                listViewLateral.Foreground = mySolidColorBrush;
                #endregion
            }        
        }
        #endregion

        public void actionControl_control(object sender, actionArg e)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                switch (e.action_type)
                {
                    case 1:
                        this.mediaControl.handle_playPause();
                        break;
                    case 2:
                        this.mediaControl.handle_stop();
                        break;
                    case 3:
                        this.mediaControl.handle_load();
                        break;
                    case 4:
                        this.mediaControl.handle_forward();
                        break;
                    default:
                        break;
                }
            }));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
