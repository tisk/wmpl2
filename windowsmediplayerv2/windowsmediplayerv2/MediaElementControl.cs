﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Windows.Threading;
using System.Collections.ObjectModel;

namespace windowsmediplayerv2
{
    class MediaElementControl
    {
        private MediaElement mediaElement1;
        private System.Windows.Controls.ListView listBox1;
        public bool isLoaded;
        private bool isPlaying;
        private Library library;
        private bool first;

        public MediaElementControl(ref MediaElement media, ref System.Windows.Controls.ListView lb, ref Library li)
        {
            isPlaying = false;
            isLoaded = false;
            this.mediaElement1 = media;
            this.listBox1 = lb;
            this.library = li;
            first = false;
        }

        public bool isInPlay()
        {
            return this.isPlaying;
        }

        public void revertPlayStatu()
        {
            this.isPlaying = !this.isPlaying;
        }

        public void stopPlay()
        {
            this.isPlaying = false;
        }

        public void unload()
        {
            isLoaded = false;
        }

        public void loadNewMedia(string mediaName)
        {
  //          if (first)
    //            mediaElement1.UnloadedBehavior = MediaState.Close;
//            mediaElement1.Close();
            if ((mediaElement1.Source = new Uri(mediaName)) != null)
                isLoaded = true;
            first = true;
        }

        #region handler
        public void handle_playPause()
        {
           if (!isLoaded)
                loadNewMedia(this.library.getCurrentPlayList().getCurrentMedia().getPath());
            if (!isLoaded)
                return;
            mediaElement1.SpeedRatio = 1;
            if (isPlaying)
            {
                mediaElement1.LoadedBehavior = MediaState.Pause;
                isPlaying = false;
            }
            else
            {
                mediaElement1.LoadedBehavior = MediaState.Play;
                isPlaying = true;
            }           
        }

        public void hpause()
        {
            mediaElement1.LoadedBehavior = MediaState.Pause;
            isPlaying = false;
        }

        public void handle_stop()
        {
            if (!isLoaded)
                return;
            mediaElement1.LoadedBehavior = MediaState.Stop;
            isPlaying = false;
        }

        public void handle_load()
        {
            isPlaying = false;
        }

        public void handle_forward()
        {
            mediaElement1.SpeedRatio += 1;
        }
        #endregion
    }
}
