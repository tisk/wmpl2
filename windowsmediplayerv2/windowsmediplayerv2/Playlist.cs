﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace windowsmediplayerv2
{
    public class Playlist
    {
        #region declarations
        [XmlElement("Name")]
        public string name { get; set; }
        public string getName() { return name; }
        public void setName(string value) { name = value; }

        private bool isRepeat;
        private int listPos;
        public ObservableCollection<AMedia> ListMedia = new ObservableCollection<AMedia>();
        public bool HasChanged;
        #endregion

        #region constructeurs
        public Playlist(string name)
        {
            listPos = 0;
            this.setRepeat(true);
            this.name = name;
            HasChanged = false;
        }

        public Playlist()
        {
            listPos = 0;
            this.setRepeat(true);
            name = "default";
            HasChanged = false;
        }
        #endregion

        public void setRepeat(bool state)
        {
            HasChanged = true;
            isRepeat = state;
        }

        public void setListPos(int id)
        {            
            if (this.getCurrentMedia() != null)
                this.getCurrentMedia().resetActionControl();
            this.listPos = id;
        }

        public bool updatePlayListPos()
        {
            int lastPos = listPos;

            if (listPos < (ListMedia.Count - 1))
                listPos++;
            else if (isRepeat == true)
                listPos = 0;
            else            
                return false;            
            this.ListMedia[lastPos].resetActionControl();           
            HasChanged = true;
            return true;
        }

        public void updatePlayListPos(int npos)
        {
            this.ListMedia[listPos].resetActionControl();
            int lastPos = listPos;
            if ((listPos + npos > 0) && (listPos + npos < ListMedia.Count))            
                listPos += npos;
            else
                listPos = 0;
            if (this.ListMedia.Count < 0)            
                listPos = 0;            
            HasChanged = true;
            return;
        }

        public void addElemToPlayList(AMedia med)
        {
            this.ListMedia.Add(med);
            this.ListMedia.Last().setId(this.ListMedia.Count - 1);
        }

        public void addEventToMedia(EventHandler<actionArg> e)
        {
            this.ListMedia[listPos].setNewActionControl(e);
        }

        public void delEventToMedia()
        {
            this.getCurrentMedia().resetActionControl();
        }

        public AMedia getCurrentMedia()
        {
            if (this.ListMedia.Count < listPos + 1)
                return null;
            return (this.ListMedia[listPos]);
        }

        public void updateIds()
        {
            for (int cnt = 0; cnt < ListMedia.Count; cnt++)
            {
                ListMedia[cnt].setId(cnt);
            }
        }

        public void delItem(int id)
        {
            this.ListMedia.RemoveAt(id);
            updateIds();
        }

        public bool isEmpty()
        {
            return ((this.ListMedia.Count == 0) ? true : false);
        }
    }
}