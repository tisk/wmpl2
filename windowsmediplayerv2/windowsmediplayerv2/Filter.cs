﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace windowsmediplayerv2
{
    public class Filter
    {
        private Expression tree = null;
        private Expression reference = null;
        public Library lib;
        private List<AMedia> result = new List<AMedia>();
        public ObservableCollection<AMedia> mediaToDraw = new ObservableCollection<AMedia>();
//        public ObservableCollection<AMedia> mediToDraw = new
        private System.Windows.Controls.ListView found;
        public bool caseSensitive = false;
        static private Dictionary<String, String> _keywords = new Dictionary<String, String>()
            {
                {"select",      "Select:"},
                {"playlist",    "playlist"},
                {"library",     "library"},
                {"type",        "Type"},
                {"music",       "music"},
                {"video",       "video"},
                {"picture",     "picture"},
                {"when",        "when"},
                {"from",          "from"},
                {"by",          "by"},
                {"name",        "name"},
                {"path",        "path"},
                {"equal",       "equal"},
                {"contains",    "contains"},
                {"notequal",    "not-equal"},
                {"notcontains", "not-contains"},
                {"notcasesensitive", "not-case-sensitive"},
                {"casesensitive", "case-sensitive"},
                {"asc",         "asc"},
                {"desc",        "desc"},
                {"and",         "and"},
                {"or",          "or"},
                {"(",           "("},
                {")",           ")"}
            };

        public Filter(ref Library library, System.Windows.Controls.ListView foundview)
        {
            lib = library;
            found = foundview;
//            foundview.ItemsSource = this.result;
        }

        static public Dictionary<String, String> keywords()
        {
            return _keywords;
        }

        static public String keyword(String key)
        {
            return _keywords[key.ToLower()];
        }

        static public String pkeyword(String key)
        {
            return pchar() + keyword(key);
        }

        static public String pchar()
        {
            return ":";
        }

        public void execute(String command)
        {
            executeSelect();
        }

        private HashSet<AMedia> getMediaAsked(Expression expr)
        {
            HashSet<AMedia> set = new HashSet<AMedia>();

            return set;
        }

        public void executeSelect()
        {
            Expression expr;
            IEnumerable<AMedia> res = null;
            Playlist playlist;

            expr = tree;
            expr = expr.getNextExpr();
            expr = (expr.getHigherPrio() != null ? expr : expr.getNextExpr());
            evalExpr(ref expr, ref res);
            answerToLogic(ref res);
            if (res != null)
            {
                if ((playlist = lib.getPlaylist("Selected")) == null)
                {
                    playlist = new Playlist("Selected");
                    lib.listPlaylist.Add(playlist);
                                    
                }
                else
                    playlist.ListMedia.Clear();
               // found.ItemsSource = lib.getPlaylist("Selected").ListMedia;
                foreach (AMedia media in res)
                {
                    System.Windows.Controls.ListViewItem item = new System.Windows.Controls.ListViewItem();
                    item.Content = media.name;
                    playlist.addElemToPlayList(media);
//                   found.Items.Add(media);
                    mediaToDraw.Add(media);
                }
            }
        }

        private void evalExpr(ref Expression expr, ref IEnumerable<AMedia> res)
        {
            Expression higher;
            while (expr != null)
            {
                higher = expr.getHigherPrio();
                evalExpr(ref higher, ref res);
                if (2 <= expr.content.Count)
                    answerToCondition(expr, ref res);
                expr = expr.getNextExpr();
            }
        }

        private bool condition(AMedia media, String condLeft, String cond, String condRight, bool casesensitive)
        {
            bool ret = false;
            String comp = "";

            if (condLeft == Filter.keyword("type"))
                comp = media.getType();
            if (condLeft == Filter.keyword("name"))
                comp = media.getName();
            if (condLeft == Filter.keyword("path"))
                comp = media.getPath();

            if (casesensitive != true)
                comp = comp.ToLower();

            if (cond.Contains(Filter.keyword("equal")) == true)
                ret = (comp == condRight ? true : false);
            if (cond.Contains(Filter.keyword("contains")) == true)
                ret = (comp.Contains(condRight) ? true : false);

            return (cond.Contains("not") == true ? !ret : ret);
        }

        private void answerToCondition(Expression expr, ref IEnumerable<AMedia> res)
        {
            String condLeft = expr.content[0].ToLower();
            String cond = expr.content[1].ToLower();
            String condRight = expr.content[2].Substring(1, expr.content[2].Length - 2);
            bool casesensitive = (keyword("CaseSensitive") == expr.content[3] ? true : false);

            if (casesensitive == false)
                condRight = condRight.ToLower();
            
            res = from media in lib.listMedia where condition(media, condLeft, cond, condRight, casesensitive) == true select media;
            expr.setResult(res);
        }

        private void answerToLogic(ref IEnumerable<AMedia> res)
        {
            Expression expr;
            expr = tree.getNextExpr();
            expr = (expr.getHigherPrio() != null ? expr : expr.getNextExpr());

            answerToPrio(ref expr, ref res);
            expr = tree;
            while (expr.getNextExpr() != null)
                expr = expr.getNextExpr();
            res = expr.getPrevExpr().getResult();
        }

        private void setLogicResult(ref Expression expr, String logicOp, IEnumerable<AMedia> res)
        {
            Expression prev;

            getNextExpr(expr).setResult(res);
            if (expr.getLowerPrio() != null)
                expr.getLowerPrio().setResult(res);
            prev = expr;
            while (prev != null && prev.content[0] == Filter.keyword(logicOp))
            {
                if ((prev = prev.getPrevExpr()) == null)
                    break;
                expr.setResult(res);
                prev = prev.getPrevExpr();
            }
        }

        private Expression getNextExpr(Expression expr)
        {
            if (expr.getNextExpr() != null && (expr.getHigherPrio() == null || expr.getPrevExpr() == null))
                return expr.getNextExpr();
            while (expr.getHigherPrio() != null)
                expr = expr.getHigherPrio();
            return expr.getLowerPrio();
        }

        private Expression getPrevExpr(Expression expr)
        {
            if (expr.getPrevExpr() != null)
                return expr.getPrevExpr();
            return expr;
        }

        private void answerToPrio(ref Expression expr, ref IEnumerable<AMedia> res)
        {
            Expression start = expr;
            Expression higher;
            while (expr != null)
            {
                if ((higher = expr.getHigherPrio()) != null)
                    answerToPrio(ref higher, ref res);
                expr = expr.getNextExpr();
            }
            answerToAnd(start, ref res);
            answerToOr(start, ref res);
        }

        private void answerToAnd(Expression expr, ref IEnumerable<AMedia> res)
        {
            while (expr != null)
            {
                if (expr.content[0] == Filter.keyword("and"))
                {
                    ObservableCollection<AMedia> tmp = new ObservableCollection<AMedia>();
                    Expression next = getNextExpr(expr);
                    Expression prev = getPrevExpr(expr);

                    foreach (AMedia media in prev.getResult())
                    {
                        if (next.getResult().Contains<AMedia>(media) == true)
                            tmp.Add(media);
                    }
                    setLogicResult(ref expr, "and", tmp);
                    prev.setResult(tmp);
                    next.setResult(tmp);
                    res = tmp;
                }
                expr = expr.getNextExpr();
            }
        }

        private void answerToOr(Expression expr, ref IEnumerable<AMedia> res)
        {
            while (expr != null)
            {
                if (expr.content[0] == Filter.keyword("or"))
                {
                    HashSet<AMedia> tmp = new HashSet<AMedia>();
                    Expression next = getNextExpr(expr);
                    Expression prev = getPrevExpr(expr);
                    foreach (AMedia media in prev.getResult())
                        tmp.Add(media);
                    foreach (AMedia media in next.getResult())
                        tmp.Add(media);
                    setLogicResult(ref expr, "or", tmp);
                    res = tmp;
                }
                expr = expr.getNextExpr();
            }
        }

        public void addInTree(String content)
        {
            List<String> tmp = new List<String>();
            Expression expr = null;

            while (content[0] == ' ')
                content = content.Substring(1);
            foreach (String token in content.Split(' '))
                if (token != "")
                    tmp.Add(token);
            if (tree == null)
            {
                tree = new Expression(tmp);
                reference = tree;
                return ;
            }
            if (content == "(")
            {
                expr = new Expression();
                reference.setHigher(expr);
                reference = expr;
                return ;
            }
            if (content == ")")
            {
                reference = reference.getLowerPrio();
                return ;
            }
            if (reference.content.Count == 0)
            {
                reference.content = tmp;
                return ;
            }
            reference.setNext(tmp);
            reference = reference.getNextExpr();
        }
    }
}
