﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace windowsmediplayerv2
{
    public class Expression
    {
        public List<String> content = new List<String>();
        private Expression higherPrio = null;
        private Expression lowerPrio = null;
        private Expression nextExpr = null;
        private Expression prevExpr = null;
        private IEnumerable<AMedia> result = new ObservableCollection<AMedia>();

        public Expression()
        {
        }

        public Expression(List<String> content)
        {
            this.content = content;
        }

        public Expression(Expression master, List<String> content, bool higher = false)
        {
            if (higher == true)
                setLower(master, content);
            else
                setPrev(master, content);
        }

        public void setLower(Expression master, List<String> content)
        {
            setLower(master);
            this.content = content;
        }

        public void setHigher(List<String> content)
        {
            new Expression(this, content, true);
        }

        public void setPrev(Expression master, List<String> content)
        {
            setPrev(master);
            this.content = content;
        }

        public void setNext(List<String> content)
        {
            new Expression(this, content);
        }

        public void setLower(Expression lower)
        {
            lowerPrio = lower;
            lower.higherPrio = this;
        }

        public void setHigher(Expression higher)
        {
            higherPrio = higher;
            higher.lowerPrio = this;
        }

        public void setPrev(Expression prev)
        {
            prevExpr = prev;
            if (prev.lowerPrio == null)
                prev.lowerPrio = lowerPrio;
            else if (lowerPrio == null)
                lowerPrio = prev.lowerPrio;
            prev.nextExpr = this;
        }

        public void setNext(Expression next)
        {
            nextExpr = next;
            if (next.lowerPrio == null)
                next.lowerPrio = lowerPrio;
            else if (lowerPrio == null)
                lowerPrio = next.lowerPrio;
            next.prevExpr = this;
        }

        public void setResult(IEnumerable<AMedia> res)
        {
            result = res;
        }

        public Expression getHigherPrio()
        {
            return higherPrio;
        }

        public Expression getLowerPrio()
        {
            return lowerPrio;
        }

        public Expression getNextExpr()
        {
            return nextExpr;
        }

        public Expression getPrevExpr()
        {
            return prevExpr;
        }

        public IEnumerable<AMedia> getResult()
        {
            return result;
        }

        override public String ToString()
        {
            String result = "";
            /*if (content.Count == 0)
                return result;*/
            Expression lower = lowerPrio;
            String tmp = "";
            bool space = false;

            while (lower != null)
            {
                result += "\t";
                lower = lower.lowerPrio;
            }
            result += "[";
            foreach (String i in content)
            {
                if (space)
                    result += " ";
                result += i;
                space = true;
            }
            result += "]";

            space = false;
            result += "(";
            foreach (AMedia i in this.result)
            {
                if (space)
                    result += "|";
                result += i.name;
                space = true;
            }
            result += ")";

            if (higherPrio != null && (tmp = higherPrio.ToString()) != "")
                result += "\n" + tmp;

            if (nextExpr != null && (tmp = nextExpr.ToString()) != "")
                result += "\n" + tmp;
            return result;
        }
    }
}
