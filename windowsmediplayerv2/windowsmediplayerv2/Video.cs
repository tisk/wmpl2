﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace windowsmediplayerv2
{
    public class Video : AMedia
    {
        public event EventHandler<actionArg> actionControl;

        public override void setNewActionControl(EventHandler<actionArg> e)
        {
            this.actionControl += e;
        }

        public override void resetActionControl()
        {
            this.actionControl = null;
        }

        public override void playPause()
        {
            base.playPause();
            if (this.actionControl != null)
                this.actionControl(this, new actionArg(1));
        }
        public override void stop()
        {
            base.stop();
            if (this.actionControl != null)
                this.actionControl(this, new actionArg(2));
        }
        public override void load()
        {
            base.load();
            if (this.actionControl != null)
                this.actionControl(this, new actionArg(3));
        }
        public override void backward()
        {
            base.backward();
            if (this.actionControl != null)
                this.actionControl(this, new actionArg(5));
        }
        public override void forwad()
        {
            base.forwad();
            if (this.actionControl != null)
                this.actionControl(this, new actionArg(4));
        }
        public override void changeVolume()
        {
            base.changeVolume();
            if (this.actionControl != null)
                this.actionControl(this, new actionArg(6));
        }
        public override int getElemType() { return 1; }
    }
}
